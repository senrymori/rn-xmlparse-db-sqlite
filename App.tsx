import React from 'react';
import Toast from 'react-native-toast-notifications';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {StyleSheet} from 'react-native';
import {Host} from 'react-native-portalize';
import {RouterApp} from './src/routes/RouterApp';

export const App = () => {
  return (
    <SafeAreaProvider style={styles.container}>
      <Host>
        <RouterApp />
      </Host>

      {/* @ts-ignore */}
      <Toast ref={ref => (global['toast'] = ref)} />
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
