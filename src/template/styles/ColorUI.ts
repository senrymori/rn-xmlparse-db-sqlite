export const ColorsUI = {
  black: '#000000',
  white: '#FFFFFF',
  seriy: '#555555',
  gray: {
    main: '#999999',
    light: '#C1C1C1',
  },
  firm: '#A5EE68',
  lightGreen: '#BFFFD1',
  red: '#870000',
  green: '#1E8A4A',
  transparent: 'transparent',
};
