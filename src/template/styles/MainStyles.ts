import {StyleSheet} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

export const Insets = useSafeAreaInsets();

export const MainStyles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  horizontal: {
    paddingHorizontal: 20,
  },
  insetsBottom: {
    paddingBottom: Insets.bottom,
  },
  insetsTop: {
    paddingTop: Insets.top,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
