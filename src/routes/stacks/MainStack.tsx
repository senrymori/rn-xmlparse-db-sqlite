import {createStackNavigator} from '@react-navigation/stack';
import {Screens} from '../models/Screens';
import {MainScreen} from '../../screens/main/MainScreen';

const Stack = createStackNavigator();

export const MainStack = () => {
  return (
    <Stack.Group>
      <Stack.Screen
        name={Screens.MAIN}
        component={MainScreen}
        options={{headerShown: false}}
      />
    </Stack.Group>
  );
};
