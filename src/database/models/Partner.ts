import {
  Entity,
  Column,
  BaseEntity,
  PrimaryGeneratedColumn,
  JoinColumn,
  OneToMany,
  OneToOne,
  ManyToOne,
} from 'typeorm';
import {PriceType} from './PriceType';

@Entity()
export class Partner extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar')
  name: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  address: string | null;

  @Column('varchar')
  phone_number: string | null;

  @ManyToOne(type => PriceType, priceType => priceType.id)
  @JoinColumn()
  price_type: PriceType;

  // @Column('uuid')
  // price_type_id: string;

  // @Column('uuid')
  // default_contract_id: string;

  // @Column('uuid')
  // default_outlet_id: string;

  // @Column('uuid')
  // default_agreement_id: string | null;
}
