import {typeORMDriver} from 'react-native-quick-sqlite';
import {DataSource} from 'typeorm';
import {Partner} from './models/Partner';
import {PriceType} from './models/PriceType';

export const dataSource = new DataSource({
  database: 'AdvanceAgent',
  entities: [Partner, PriceType],
  location: '.',
  logging: [],
  synchronize: true,
  type: 'react-native',
  driver: typeORMDriver,
});
