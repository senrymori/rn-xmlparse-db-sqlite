import React, {useEffect} from 'react';
import {CenterContainerFlex} from '../../template/containers/CenterContainer';
import {ColorsUI} from '../../template/styles/ColorUI';
import {ButtonUI} from '../../template/ui/ButtonUI';
import {startParseXML} from '../../actions/SaxHelper';
import {useStore} from 'effector-react';
import {$xmlData} from '../../modules/xml/xml-store';
import {Ag, TextUI} from '../../template/ui/TextUI';
import {MainContainer} from '../../template/containers/MainContainer';
import {ScrollView} from 'react-native';
import {createPartner, getPartners} from './main-utils';
import {$partners} from '../../modules/partners/partners-store';

export const MainScreen: React.FC = function MainScreen() {
  const xmlData = useStore($xmlData);
  const partners = useStore($partners);

  return (
    <ScrollView contentContainerStyle={{flex: 1}}>
      <CenterContainerFlex $ph={10} $bg={ColorsUI.white}>
        <MainContainer style={{borderWidth: 1}} $ph={10} $pv={10}>
          {xmlData.map((data, index) => (
            <TextUI key={`${index}`} ag={Ag['400_16']}>
              {data.name}
            </TextUI>
          ))}
        </MainContainer>

        <ButtonUI $mt={10} title={'Parse XML'} onPress={startParseXML} />

        <ButtonUI
          $mt={10}
          title={'Create Partner to table'}
          onPress={createPartner}
        />

        <ButtonUI
          $mt={10}
          $mb={10}
          title={'Get data from table'}
          onPress={getPartners}
        />

        {partners.map(partner => (
          <TextUI
            $mb={10}
            style={{borderWidth: 1, paddingHorizontal: 10, paddingVertical: 10}}
            key={partner.id}
            ag={Ag['400_16']}>
            {`id ${partner.id}:\n${partner.name} ${partner.phone_number}\n${partner.price_type.name}`}
          </TextUI>
        ))}
      </CenterContainerFlex>
    </ScrollView>
  );
};
