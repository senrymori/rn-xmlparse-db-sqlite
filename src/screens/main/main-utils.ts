import {PriceType} from './../../database/models/PriceType';
import {dataSource} from '../../database/Database';
import {Partner} from '../../database/models/Partner';
import {addPartnersList} from '../../modules/partners/partners-store';

export const createPartner = async () => {
  const partnerRepository = dataSource.getRepository(Partner);
  const priceTypeRepository = dataSource.getRepository(PriceType);

  let prices = await priceTypeRepository.find();

  if (!prices.length) {
    const price = priceTypeRepository.create({
      id: Math.random().toString(),
      name: 'KZT',
    });

    await price.save();

    prices = await priceTypeRepository.find();
  }

  const partner = partnerRepository.create({
    id: Math.random().toString(),
    name: 'John Smith',
    address: 'st. Bleach 20',
    phone_number: '+77005009090',
    price_type: prices[0],
  });

  await partner.save();
};

export const getPartners = async () => {
  const partnerRepository = dataSource.getRepository(Partner);

  const partners = await partnerRepository.find({
    relations: {
      price_type: true,
    },
  });

  console.log(partners);

  addPartnersList(partners);
};
