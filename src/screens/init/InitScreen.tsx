import React, {useEffect} from 'react';
import {ColumnContainerFlexCenter} from '../../template/containers/ColumnContainer';
import {ColorsUI} from '../../template/styles/ColorUI';
import Navigation from '../../routes/navigation/Navigation';
import {Screens} from '../../routes/models/Screens';
import {dataSource} from '../../database/Database';
import {getPartners} from '../main/main-utils';

export const InitScreen: React.FC = function InitScreen() {
  useEffect(() => {
    const connect = async () => {
      await dataSource.initialize();

      await getPartners();
    };

    connect();

    setTimeout(() => {
      Navigation.navigate(Screens.MAIN);
    }, 2000);
  }, []);

  return (
    <ColumnContainerFlexCenter $bg={ColorsUI.white}>
      <></>
    </ColumnContainerFlexCenter>
  );
};
