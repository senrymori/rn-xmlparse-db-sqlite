import ReactNativeBlobUtil from 'react-native-blob-util';
import {SAXParser} from 'sax';
import {addXmlDataParsing} from '../modules/xml/xml-store';

interface ChunkProps {
  chunk: any;
}

export const startParseXML = async () => {
  const filePath = `${ReactNativeBlobUtil.fs.dirs.MainBundleDir}/ToMobile.xml`;
  await ReactNativeBlobUtil.fs.readStream(filePath, 'utf8').then(res => {
    res.open();

    return res.onData(chunk => {
      parseXML({chunk});
    });
  });
};

const saxStream = new SAXParser(true);

saxStream.onopentag = tag => {
  addXmlDataParsing(tag);
};

const parseXML = ({chunk}: ChunkProps) => {
  saxStream.write(chunk);
};
