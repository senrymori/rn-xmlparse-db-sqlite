import {Partner} from '../../database/models/Partner';
import {createStore, createEvent} from 'effector';

export const addPartnersList = createEvent<Partner[]>();

export const $partners = createStore<Partner[]>([]).on(
  addPartnersList,
  (_, value) => value,
);
