import {createEvent, createStore} from 'effector';

export interface XmlParseData {
  attributes: any;
  isSelfClosing: boolean;
  name: string;
}

export const addXmlDataParsing = createEvent<XmlParseData>();

export const $xmlData = createStore<XmlParseData[]>([]).on(
  addXmlDataParsing,
  (state, value) => [...state, value],
);
